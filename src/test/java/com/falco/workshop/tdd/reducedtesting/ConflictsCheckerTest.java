package com.falco.workshop.tdd.reducedtesting;

import com.falco.workshop.tdd.ConflictsChecker;
import com.falco.workshop.tdd.Slot;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.IntStream;

import static com.falco.workshop.tdd.Slot.slot;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class ConflictsCheckerTest {

    private ConflictsChecker conflictsChecker;

    @Before
    public void setUp() throws Exception {
        conflictsChecker = new ConflictsChecker();
    }

    @Test
    public void shouldDetectConflict() {
        assertThat(conflictsChecker.checkConflicts(asList(
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:10-12:15"))
        )).containsOnly(slot("2018-01-01", "12:00-12:15"), slot("2018-01-01", "12:10-12:15"));
    }

    @Test
    public void shouldNotDetectConflictForAdjacentSlots() {
        assertThat(conflictsChecker.checkConflicts(asList(
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:15-12:30"))
        )).isEmpty();
    }

    @Test
    public void shouldDetectDuplicateConflict() {
        assertThat(conflictsChecker.checkConflicts(asList(
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:00-12:15"))
        )).containsExactlyInAnyOrder(
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:00-12:15"));
    }

    @Test
    public void shouldDetectMassiveConflicts() {
        List<Slot> slots = IntStream.range(1, 1000).mapToObj(i -> slot("2018-10-10", "12:00-12:15")).collect(toList());
        List<Slot> actual = conflictsChecker.checkConflicts(slots);
        assertThat(actual).containsExactlyElementsOf(slots);
    }
}
