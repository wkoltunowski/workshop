package com.falco.workshop.tdd.reducedtesting;

import com.falco.workshop.tdd.SlotScheduler;
import org.junit.Before;
import org.junit.Test;

import static com.falco.workshop.tdd.Slot.slot;
import static org.assertj.core.api.Assertions.assertThat;


public class GenerateSlotsTest {

    private SlotScheduler scheduler;

    @Before
    public void setUp() {
        scheduler = new SlotScheduler();
    }

    @Test
    public void shouldScheduleOneSlot() {
        assertThat(scheduler.generateDurationSlots(
                "2018-01-01T12:00", "2018-01-01T12:15",
                "PT15M")
        ).containsExactly(slot("2018-01-01", "12:00-12:15"));
    }

    @Test
    public void shouldSchedule4Slots() {
        assertThat(scheduler.generateDurationSlots(
                "2018-01-01T12:00", "2018-01-01T13:00",
                "PT15M")
        ).containsExactly(
                slot("2018-01-01", "12:00-12:15"),
                slot("2018-01-01", "12:15-12:30"),
                slot("2018-01-01", "12:30-12:45"),
                slot("2018-01-01", "12:45-13:00")
        );
    }

    @Test
    public void shouldNotScheduleSlotsWhenPeriodTooSmall() {
        assertThat(scheduler.generateDurationSlots("2018-01-01T12:00", "2018-01-01T12:10", "PT15M")).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenFromAfterTo() {
        scheduler.generateDurationSlots("2018-02-01T12:00", "2018-01-01T12:10", "PT15M");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseErrorWhenWrongValidFrom() {
        scheduler.generateDurationSlots("2018-01-32T12:00", "2018-02-01T12:10", "PT15M");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseErrorWhenWrongValidTo() {
        scheduler.generateDurationSlots("2018-01-01T12:00", "2018-02-30T12:10", "PT15M");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenFromNull() {
        scheduler.generateDurationSlots(null, "2018-01-01T12:10", "PT15M");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenToNull() {
        scheduler.generateDurationSlots("2018-01-01T12:00", "2018-01-01T12:10", "PT15M");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenDurationNull() {
        scheduler.generateDurationSlots("2018-01-01T12:00", null, null);
    }
}