package com.falco.workshop.tdd.reducedtesting;

import com.falco.workshop.tdd.Slot;
import com.falco.workshop.tdd.SlotMerger;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.falco.workshop.tdd.Slot.slot;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class SlotMergerTest {

    private SlotMerger slotMerger;

    @Before
    public void setUp() {
        slotMerger = new SlotMerger();
    }

    @Test
    public void shouldMergeAdjacentSlots() {
        Slot s1 = slot("2018-01-01", "08:00-08:15");
        Slot s2 = slot("2018-01-01", "08:15-08:30");
        assertThat(slotMerger.mergeSlots(s1, s2)).containsExactly(slot("2018-01-01", "08:00-08:30"));
    }

    @Test
    public void shouldMergeOne() {
        Slot s1 = slot("2018-01-01", "08:00-08:15");
        assertThat(slotMerger.mergeSlots(s1)).containsExactly(slot("2018-01-01", "08:00-08:15"));
    }

    @Test
    public void shouldMergeOverlappingSlots() {
        Slot s1 = slot("2018-01-01", "08:00-08:15");
        Slot s2 = slot("2018-01-01", "08:10-08:30");
        assertThat(slotMerger.mergeSlots(s1, s2)).containsExactly(slot("2018-01-01", "08:00-08:30"));
    }

    @Test
    public void shouldMergeIntoTwo() {
        Slot s1 = slot("2018-01-01", "08:00-08:15");
        Slot s2 = slot("2018-01-01", "08:15-08:30");
        assertThat(slotMerger.mergeSlots(s1, s2)).containsExactly(slot("2018-01-01", "08:00-08:30"));
    }


    @Test
    public void shouldNotModifyInput() {
        Slot s1 = slot("2018-01-01", "08:00-08:15");
        Slot s2 = slot("2018-01-01", "08:15-08:30");
        slotMerger.mergeSlots(s1, s2);
        assertThat(s1).isEqualTo(slot("2018-01-01", "08:00-08:15"));
        assertThat(s2).isEqualTo(slot("2018-01-01", "08:15-08:30"));
    }

    private List<Slot> oneMegaSlots = emptyList();//IntStream.range(1, 1000000).mapToObj(i -> Slot.slot("2018-01-01", "12:00-12:15")).collect(toList());

    @Test
    public void shouldCopySlots() {
        oneMegaSlots.stream().map(s -> Slot.slot(s.date(), s.hours())).collect(toList());
    }
}
