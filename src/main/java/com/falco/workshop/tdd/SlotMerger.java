package com.falco.workshop.tdd;

import com.google.common.collect.TreeRangeSet;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class SlotMerger {
    public Set<Slot> mergeSlots(Slot... slots) {
        TreeRangeSet<LocalDateTime> rangeSet = TreeRangeSet.create();
        Arrays.stream(slots).forEach(s -> rangeSet.add(s.range()));

        return rangeSet.asRanges().stream().map(range -> {
            LocalDateTime from = range.lowerEndpoint();
            LocalDateTime to = range.upperEndpoint();
            return Slot.slot(from.toLocalDate().toString(), from.toLocalTime().toString() + "-" + to.toLocalTime().toString());
        }).collect(Collectors.toSet());
    }
}
