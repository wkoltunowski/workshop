package com.falco.workshop.tdd;

import com.google.common.collect.Range;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Slot {
    private final LocalDate day;
    private final LocalTime hoursFrom;
    private final LocalTime hoursTo;

    public Slot(String date, String hours) {
        this.day = LocalDate.parse(date);
        this.hoursFrom = LocalTime.parse(hours.split("-")[0]);
        this.hoursTo = LocalTime.parse(hours.split("-")[1]);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
    }

    public static Slot slot(String date, String hours) {
        return new Slot(date, hours);
    }

    public String date() {
        return day.toString();
    }

    public String hours() {
        return hoursFrom.toString() + "-" + hoursTo.toString();
    }

    public Range<LocalDateTime> range() {
        return Range.closedOpen(day.atTime(hoursFrom), day.atTime(hoursTo));
    }
}
