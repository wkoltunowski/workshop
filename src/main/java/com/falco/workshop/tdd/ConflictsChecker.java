package com.falco.workshop.tdd;

import com.google.common.collect.Range;
import com.google.common.collect.TreeRangeSet;

import java.time.LocalDateTime;
import java.util.*;

import static java.util.stream.Collectors.toList;

public class ConflictsChecker {
    public List<Slot> checkConflicts(List<Slot> slots) {
        return checkGuavaRanges(slots);
//        return checkConflictsOnsqr(slots);
    }

    private List<Slot> checkConflictsOnsqr(List<Slot> slots) {
        List<Slot> conflicts = new LinkedList<>();
        for (int i = 0; i < slots.size(); i++) {
            for (int j = 0; j < slots.size(); j++) {
                Slot slot1 = slots.get(i);
                Slot slot2 = slots.get(j);
                if (i != j && slot1.range().isConnected(slot2.range()) && !slot1.range().intersection(slot2.range()).isEmpty()) {
                    conflicts.add(slot1);
                    break;
                }
            }
        }
        return conflicts;
    }

    private List<Slot> checkGuavaRanges(List<Slot> slots) {
        TreeRangeSet<LocalDateTime> rangeSet = TreeRangeSet.create();
        //need to transform to closed range due to isConnected issues between [a,b) and [b,c) (which are connected and coalesced in TreeRangeSet)
        slots.forEach(s -> rangeSet.add(Range.closed(s.range().lowerEndpoint(), s.range().upperEndpoint().minusMinutes(1))));
        Map<Range<LocalDateTime>, List<Slot>> map = new HashMap<>();
        for (Slot slot : slots) {
            Range<LocalDateTime> range = rangeSet.rangeContaining(slot.range().lowerEndpoint());
            List<Slot> rangeSlots = map.getOrDefault(range, new LinkedList<>());
            rangeSlots.add(slot);
            map.putIfAbsent(range, rangeSlots);
        }
        return map.values().stream().filter(l -> l.size() > 1).flatMap(Collection::stream).collect(toList());
    }
}
