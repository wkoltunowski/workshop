package com.falco.workshop.tdd;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SlotScheduler {
    public List<Slot> generateDurationSlots(String from, String to, String duration) {
        Duration slotDuration = Duration.parse(duration);
        List<Slot> slots = new ArrayList<>();
        LocalDateTime slotDate = LocalDateTime.parse(from);
        while (slotDate.isBefore(LocalDateTime.parse(to))) {
            slots.add(Slot.slot(slotDate.toLocalDate().toString(),
                    slotDate.toLocalTime().toString() + "-" + slotDate.toLocalTime().plus(slotDuration)));
            slotDate = slotDate.plus(slotDuration);
        }
        return slots;
    }
}
